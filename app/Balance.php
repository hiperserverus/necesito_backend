<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Country;

class Balance extends Model

{

    const BALANCE_INITIAL = 0.00;
    
    protected $table= 'balances';

    protected $fillable= ['value', 'user_id', 'country_id'];

    public function user() {

        return $this->belongsTo(User::class);
    }

    public function country() {

        return $this->hasOne(Country::class);
    }
}
