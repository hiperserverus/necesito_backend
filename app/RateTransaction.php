<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Transaction;
use App\Moneda;

class RateTransaction extends Model
{
    protected $table = 'rate_transactions';

    protected $fillable = [ 'amount', 'min_quantity', 'before_quantity', 'buy_rate','sell_rate'];

    public function transaction() {

        return $this->hasOne(Transaction::class);
    }

}
