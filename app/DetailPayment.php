<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Transaction;
use App\BankAccount;
use App\ServiceAllow;
use App\PaymentGetaway;

class DetailPayment extends Model
{
    protected $table = 'detail_payments';
    protected $fillable = ['image'];

    public function transaction() {

        return $this->hasOne(Transaction::class);
    }

    public function paymentGetaway() {
        return $this->belongsTo(PaymentGetaway::class);
    }

    public function bankAccountDestination() {
        return $this->belongsTo(BankAccount::class, 'bank_account_destination_id');
    }

    public function bankAccountSeller() {
        return $this->belongsTo(BankAccount::class, 'bank_account_seller_id');
    }

    public function serviceAllow() {

        return $this->belongsto(ServiceAllow::class);
    }
}
