<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Product;
use App\DetailPayment;
use App\ServiceType;
use App\PaymentMethod;
use App\Rate;

class ServiceAllow extends Model
{

    const STATUS_OPTIONS = ['active', 'inactive'];
    const INITIAL_STATE = 'active';

    protected $table = 'service_allows';
    protected $fillable = ['product_id'];

    public function product() {

        return $this->belongsTo(Product::class);
    }

    public function detailPayment() {

        return $this->hasOne(DetailPayment::class);
    }

    public function serviceType() {
        
        return $this->belongsTo(ServiceType::class);
    }

    public function paymentMethod() {

        return $this->belongsTo(PaymentMethod::class);
    }

    public function rates() {

        return $this->hasMany(Rate::class);
    }
}
