<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Rate;
use App\RateTransaction;
use App\Country;

class Moneda extends Model
{
    protected $table ='Monedas';
    protected $fillable = ['name'];


    public function rateTransaction() {

        return $this->hasOne(RateTransaction::class);
    }

    public function rates() {

        return $this->hasMany(Rate::class);
    }

    public function country() {

        return $this->belongsTo(Country::class);
    }
}
