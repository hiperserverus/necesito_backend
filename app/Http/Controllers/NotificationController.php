<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;

class NotificationController extends Controller
{
   public function create(Request $request) {

    $APP_ID = 'e17f039b-73e8-4f7f-9099-b7d072465c60';
       $API_KEY = 'MmZhNTYzYzItMDJjMy00YjRmLTg1ZDMtZDZiMTA2ODY2NWQw';

    // $client = new Client(['base_uri' => 'https://myapp.com/api/']);
      $client = new Client();

    $response = $client->post("https://onesignal.com/api/v1/notifications", [
        // un array con la data de los headers como tipo de peticion, etc.
        'headers' => ['Content-Type' => 'application/json',
                        'Authorization' => 'Basic '.$API_KEY],
        // array de datos del formulario
        'json' => [
            'app_id' => $APP_ID,
            'included_segments' => ["Active Users", "Inactive Users"],
            'data' => ["postId" => $request->postId],
            'contents' => ["es" => $request->message, "en" => $request->message],
            'headings' => ["es" => $request->title, "en" => $request->message]
            // "big_picture" => Storage::disk('images')->get($request->nameImage),
            // "large_icon" => Storage::disk('images')->get($request->nameImage),
        ]
    ]);


            return response()->json(['notificacion' => $response]);
    }
}
