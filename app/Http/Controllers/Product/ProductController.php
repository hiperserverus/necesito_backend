<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\ApiController;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Collection;

use Carbon\Carbon;

class ProductController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $products = Product::all();

       // $products = Product::orderBy('created_at', 'desc')->with('seller.rates')->paginate(5);

       $products = Product::inRandomOrder()->with('seller.ubication.country', 
       'category')->paginate(5);

          // $products = Product::inRandomOrder()->paginate(5);
        
        // $products = DB::table('products')
        // ->orderBy('created_at', 'desc')
        // ->paginate(10);
         
        

          return response()->json(['products' => $products]);


        //  return $this->showall($products);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $product = Product::findOrFail($id)->with('seller', 'category')->findOrFail($id);

         return response()->json(['product' => $product]);
        // return $this->showOne($product);
    }

}
