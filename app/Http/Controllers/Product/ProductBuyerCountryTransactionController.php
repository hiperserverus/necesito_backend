<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\ApiController;

use App\User;
use App\Buyer;
use App\Product;
use App\Rate;
use App\Transaction;
use App\RateTransaction;
use App\DetailPayment;
use App\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductBuyerCountryTransactionController extends ApiController
{


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Product $product, User $buyer, Country $country)
    {


        // $rules = [
        //     'amount' => 'required|integer',
        // ];

        // $this->validate($request, $rules);

        if (!$request->has('amount')) {

            return $this->errorResponse('El monto es requerido para realizar esta transaccíon',409);

        }

        if($buyer->id == $product->seller_id) {
            return $this->errorResponse('El comprador debe ser diferente al vendedor',409);
        }
        
        if(!$buyer->esVerificado()) {
            return $this->errorResponse('El comprador debe ser un usuario verificado', 409);
        }

        if( !$product->seller->esVerificado()) {
            return $this->errorResponse('El vendedor debe ser un usuario verificado', 409);
        }

        if (!$product->estaDisponible()) {
            return $this->errorResponse('El producto para esta transaccion no esta disponible', 409);
        }

        $seller = $product->seller;

        $rate =  $seller->rates()->where('country_id', $country->id)->get();


        if($rate->count() == 0) {
            return $this->errorResponse('No se encontro cotizacion para el país seleccionado', 409);
        }


        if ($rate[0]->quantity < $request->amount) {
            return $this->errorResponse('El producto no tiene la cantidad disponible requerida para esta transaccion', 409);
        }

        if ( $request->amount < $rate[0]->min_quantity) {
            return $this->errorResponse('La cantidad solicitada para esta transaccion no esta permitida', 409);
        }


        // if (!$request->has('image')) {

        //     return $this->errorResponse('Es necesario que cargue el comprobante para completar la transacción...', 409);
        // }

        $request->status = Transaction::DEFAULT_STATUS;

        return DB::transaction(function () use ($request, $product, $buyer, $country, $rate) {
            
            $before_quantity = $rate[0]->quantity;
            
            $rate[0]->quantity -= $request->amount;



            $rate[0]->save();

            $detial_payment = DetailPayment::create([
                'image' => $request->image,
                'payment_holder_account' => $request->holder_account,
                'payment_indetifier' => $request->payment_indetifier,
                'bank_account_destination_id' => $request->bank_destination,
                'bank_account_seller_id' => $request->bank_seller,
                'payment_method_id' => $request->payment_method
            ]);

            $rate_transaction = RateTransaction::create([
                'before_quantity' => $before_quantity,
                'min_quantity' => $rate[0]->min_quantity,
                'amount' => $request->amount,
                'buy_rate' => $rate[0]->buy_rate,
                'sell_rate' => $rate[0]->sell_rate,
                'country_id' => $country->id
                
            ]);

            $transaction = Transaction::create([
                
                'image' => null,
                'status' => $request->status,
                'buyer_id' => $buyer->id,
                'product_id' => $product->id,
                'rate_transaction_id' => $rate_transaction->id,
                'detail_payment_id' => $detial_payment->id,
            ]);

        

            return $this->showOne($transaction, 201);

        });

    }

    
}
