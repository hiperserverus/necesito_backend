<?php

namespace App\Http\Controllers\Rate;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;

use App\Rate;

class RateController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rates = Rate::all();

        return $this->showAll($rates);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Rate $rate)
    {
        return $this->showOne($rate);
    }


}
