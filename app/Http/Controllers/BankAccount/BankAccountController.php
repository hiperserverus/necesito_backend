<?php

namespace App\Http\Controllers\BankAccount;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;

use App\BankAccount;

class BankAccountController extends ApiController
{
   
    public function index()
    {
        $bankAccounts = BankAccount::all();

        return $this->showAll($bankAccounts);
    }

    public function show($id)
    {

        $bankAccount = BankAccount::findOrFail($id);


        return $this->showOne($bankAccount);
    }

  
}
