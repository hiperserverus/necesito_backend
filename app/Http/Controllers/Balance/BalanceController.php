<?php

namespace App\Http\Controllers\Balance;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;

use App\Balance;

class BalanceController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $balances = Balance::all();

        return $this->showAll($balances);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Balance $balance)
    {
        return $this->showOne($balance);
    }

  
}
