<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BankAccount;
use App\User;

class Whitdraw extends Model
{
    const DEFAULT_STATUS='pendiente';
    const STATUS_OPTIONS = ['pendiente', 'aprobado', 'rechazado'];
    

    protected $table= 'whitdraws';
    protected $fillable = ['amount', 'status'];

    public function user() {

        return $this->belongsTo(User::class);
    }

    public function bank_account() {

        return $this->belongsTo(BankAccount::class);
    }
}
