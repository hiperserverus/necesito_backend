<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\ServiceAllow;


class PaymentMethod extends Model
{
    protected $table = 'payment_methods';
    protected $fillable = ['payment_method_name'];

    public function serviceAllow() {

        return $this->hasMany(ServiceAllow::class);
    }
  
}
