<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Seller;
use App\Country;

class Ubication extends Model
{
    protected $table = 'ubications';
    protected $fillable = ['name','code'];


    public function seller() {

        return $this->belongsTo(Seller::class);
    }

    public function country() {

        return $this->belongsTo(Country::class);
    }
}
