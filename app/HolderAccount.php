<?php

namespace App;

use App\User;
use App\BankAccount;
use App\PaymentGetaway;

use Illuminate\Database\Eloquent\Model;

class HolderAccount extends Model
{
    protected $table='holder_accounts';
    protected $fillable=['holder_account_dni', 'holder_account_name' ];

    public function user() {

        return $this->belongsTo(User::class);
    }

    public function bankAccounts() {

        return $this->hasMany(BankAccount::class);
    }

    public function paymentGetaways() {

        return $this->hasMany(PaymentGetaway::class);
    }
}
