@component('mail::message')
# Hola {{ $user->name }}

Tu cuenta ha sido creada exitosamente. Porfavor verificala usando el siguiente boton:

@component('mail::button', ['url' => route('verify', $user->verification_token)])
Confirmar cuenta
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent