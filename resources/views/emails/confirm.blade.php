@component('mail::message')
# Hola {{ $user->name }}

Cambio de correo electrónico realizado exitosamente. Porfavor verificalo usando el siguiente boton:

@component('mail::button', ['url' => route('verify', $user->verification_token)])
Confirmar nuevo correo
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent