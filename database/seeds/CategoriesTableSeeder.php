<?php

use Illuminate\Database\Seeder;

use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'ELECTRICIDAD',
            'GRAFITERIA',
            'SERVICIO TECNICO',
            'COSTURERIA',
            'PASTELERIA',
            'PELUQUERIA',
            'LAVANDERIA',
            'FERRETERIA',
            'DELIVERY',
            'ACCESORIOS ELECTRONICOS',
            'TIENDAS DE ROPA',
            'MAESTROS',
            'ABOGADOS',
            'ODONTOLOGOS',
            'DOCTORES'
            
        ];

        for ($i=0; $i < count($categories); $i++) {

            $category = new Category();
            $category->name = utf8_encode(ucwords(strtolower($categories[$i])));
            

            $category->save();
        }
    }
}
