<?php

use Illuminate\Database\Seeder;

use App\Runway;

class RunwaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $runways = [
            'PAYPAL',
            'PAYEER',
            'PAYONNER',
            'NETELLER',
            'SKRILL',
            'YAPE',
            'BITCOIN'       
        ];

        for ($i=0; $i < count($runways); $i++) {

            $runway = new Runway();
            $runway->runway_name = utf8_encode(ucwords(strtolower($runways[$i])));
       

            $runway->save();
        }
    }
}
