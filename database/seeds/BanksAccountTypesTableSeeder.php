<?php

use Illuminate\Database\Seeder;

use App\BankAccountType;

class BanksAccountTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            'AHORRO',
            'CORRIENTE'          
        ];

        for ($i=0; $i < count($types); $i++) {

            $type = new BankAccountType();
            $type->bank_account_type_name = utf8_encode(ucwords(strtolower($types[$i])));
       

            $type->save();
        }
    }
}
