<?php

use Illuminate\Database\Seeder;

use App\Bank;

class BanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $banks = [
            'BANESCO',
            'PROVINCIAL',
            'VENEZUELA',
            'MERCANTIL',
            'BICENTENARIO',
            'BOD',
            'TESORO',
            'BCP',
            'SCOTIABANK'
        ];

        for ($i=0; $i < count($banks); $i++) {

            $bank = new Bank();
            $bank->bank_name = utf8_encode(ucwords(strtolower($banks[$i])));
       

            $bank->save();
        }
    }
}
