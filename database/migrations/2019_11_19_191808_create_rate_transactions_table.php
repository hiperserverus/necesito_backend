<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rate_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('amount', 8, 8);
            $table->float('min_quantity', 8, 8);
            
            $table->float('before_quantity', 8, 8);
            $table->float('buy_rate', 8, 2);
            $table->float('sell_rate', 8, 2);
            
            $table->unsignedBigInteger('moneda_emitter_id');
            $table->unsignedBigInteger('moneda_recepter_id');
            $table->timestamps();


            $table->foreign('moneda_emitter_id')->references('id')->on('monedas');
            $table->foreign('moneda_recepter_id')->references('id')->on('monedas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rate_transactions');
    }
}
