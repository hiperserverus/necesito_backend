<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ServiceAllow;

class CreateServiceAllowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_allows', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('service_type_id');
            $table->unsignedBigInteger('payment_method_id');
            $table->enum('status',ServiceAllow::STATUS_OPTIONS)->default(ServiceAllow::INITIAL_STATE);


            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('service_type_id')->references('id')->on('service_types');
            $table->foreign('payment_method_id')->references('id')->on('payment_methods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_allows');
    }
}
