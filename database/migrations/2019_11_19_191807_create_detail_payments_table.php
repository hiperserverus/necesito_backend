<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('image')->nullable();
            $table->unsignedBigInteger('payment_getaway_id')->nullable();
            $table->unsignedBigInteger('bank_account_destination_id')->nullable();
            $table->unsignedBigInteger('bank_account_seller_id')->nullable();
            $table->unsignedBigInteger('service_allow_id')->nullable();

            $table->timestamps();

            $table->foreign('payment_getaway_id')->references('id')->on('payment_getaways');
            $table->foreign('bank_account_destination_id')->references('id')->on('bank_accounts');
            $table->foreign('bank_account_seller_id')->references('id')->on('bank_accounts');
            $table->foreign('service_allow_id')->references('id')->on('service_allows');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_payments');
    }
}
