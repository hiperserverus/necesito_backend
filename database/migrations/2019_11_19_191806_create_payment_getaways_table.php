<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\PaymentGetaway;

class CreatePaymentGetawaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_getaways', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->float('amount_disponibility', 8, 2)->default(PaymentGetaway::INITIAL_AMOUNT);
            $table->string('payment_identifier');


            $table->unsignedBigInteger('runway_id');
            $table->unsignedBigInteger('holder_account_id');

            $table->enum('status',PaymentGetaway::STATUS_OPTIONS)->default(PaymentGetaway::INITIAL_STATE);

            $table->unsignedBigInteger('seller_id');

            
            $table->timestamps();

           
            $table->foreign('runway_id')->references('id')->on('runways');
            $table->foreign('holder_account_id')->references('id')->on('holder_accounts');
            $table->foreign('seller_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_getaways');
    }
}
