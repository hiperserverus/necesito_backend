<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Transaction;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('transaction_identifier')->nullable();
            $table->string('image')->nullable();
            $table->string('image2')->nullable();
            $table->string('image3')->nullable();
            $table->string('description')->nullable();
            $table->enum('status',Transaction::STATUS_OPTIONS)->default(Transaction::DEFAULT_STATUS);

            $table->unsignedBigInteger('buyer_id');
            $table->unsignedBigInteger('product_id');

            $table->unsignedBigInteger('rate_transaction_id');
            $table->unsignedBigInteger('detail_payment_id');
  

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('buyer_id')->references('id')->on('users');
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('rate_transaction_id')->references('id')->on('rate_transactions');
            $table->foreign('detail_payment_id')->references('id')->on('detail_payments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
