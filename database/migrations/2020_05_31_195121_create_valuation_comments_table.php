<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateValuationCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('valuation_comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('value');
            $table->string('comment', 600);
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('buyer_id');

            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('buyer_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('valuation_comments');
    }
}
