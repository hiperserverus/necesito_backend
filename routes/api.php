<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

/**
 *  Notifications
 */

Route::post('notification', 'NotificationController@create');

/**
 * Images
 */


Route::get('image/{filename}', 'PhotoController@index');
Route::post('image', 'PhotoController@create');

/**
 * Users
 */

Route::resource('users', 'User\UserController', ['except' => ['create', 'edit']]);
Route::resource('users.balances', 'User\UserBalanceController', ['except' => ['create', 'show', 'edit', 'store']]);
Route::resource('users.bankaccounts', 'User\UserBankAccountController', ['except' => ['create', 'edit']]);
Route::resource('users.countrys.bankaccounts', 'User\UserCountryBankAccountController', ['except' => ['create', 'show', 'edit']]);

Route::name('verify')->get('users/verify/{token}', 'User\UserController@verify');
Route::name('resend')->get('users/{user}/resend', 'User\UserController@resend');

/**
 * Buyers
 */

Route::resource('buyers', 'Buyer\BuyerController', ['only' => ['index', 'show']]);
Route::resource('buyers.products', 'Buyer\BuyerProductController', ['only' => ['index']]);
Route::resource('buyers.transactions', 'Buyer\BuyerTransactionController', ['only' => ['index']]);
Route::resource('buyers.categories', 'Buyer\BuyerCategoryController', ['only' => ['index']]);
Route::resource('buyers.sellers', 'Buyer\BuyerSellerController', ['only' => 'index']);

/**
 * Sellers
 */

 Route::resource('sellers', 'Seller\SellerController', ['only' => ['index', 'show']]);
 Route::resource('sellers.transactions', 'Seller\SellerTransactionController', ['only' => ['index']]);
 Route::resource('sellers.categories', 'Seller\SellerCategoryController', ['only' => ['index']]);
 Route::resource('sellers.buyers', 'Seller\SellerBuyerController', ['only' => 'index']);
 Route::resource('sellers.products', 'Seller\SellerProductController', ['except' => 'create', 'show', 'edit']);
 Route::resource('sellers.rates', 'Seller\SellerRateController', ['except' => 'create', 'show', 'edit']);


 /**
 * Categories
 */

 Route::resource('categories', 'Category\CategoryController', ['except' => ['create', 'edit']]);
 Route::resource('categories.products', 'Category\CategoryProductController', ['only' => ['index']]);
 Route::resource('categories.sellers', 'Category\CategorySellerController', ['only' => ['index']]);
 Route::resource('categories.transactions', 'Category\CategoryTransactionController', ['only' => ['index']]);
 Route::resource('categories.buyers', 'Category\CategoryBuyerController', ['only' => ['index']]);
 

/**
 * Products
 */

 Route::resource('products', 'Product\ProductController', ['only' => ['index', 'show']]);
 Route::resource('products.transactions', 'Product\ProductTransactionController', ['only' => ['index']]);
 Route::resource('products.buyers', 'Product\ProductBuyerController', ['only' => ['index']]);
 Route::resource('products.categories', 'Product\ProductCategoryController', ['only' => ['index', 'update', 'destroy']]);
 Route::resource('products.buyers.countrys.transactions', 'Product\ProductBuyerCountryTransactionController', ['only' => ['store']]);
 Route::resource('products.countrys.transactions', 'Product\ProductCountryTransactionController', ['only' => ['index']]);

 /**
  * Transactions
  */

Route::resource('transactions', 'Transaction\TransactionController', ['only' => ['index', 'show']]);
Route::resource('transactions.categories', 'Transaction\TransactionCategoryController', ['only' => ['index']]);
Route::resource('transactions.sellers', 'Transaction\TransactionSellerController', ['only' => ['index']]);


/**
 * Rates
 */

 Route::resource('rates', 'Rate\RateController', ['only' => ['index', 'show']]);

 /**
  * Countrys
  */

  Route::resource('countrys', 'Country\CountryController', ['except' => ['create', 'edit']]);
  Route::resource('countrys.bankaccounts', 'Country\CountryBankAccountController', ['except' => ['create', 'show', 'edit']]);

/**
  * Monedas
  */

  Route::resource('monedas', 'Moneda\MonedaController', ['except' => ['create', 'edit']]);



  /**
   * Balances
   */

   Route::resource('balances', 'Balance\BalanceController', ['only' => ['index', 'show']]);

  /**
   * BankAccounts
   */

   Route::resource('bankaccounts', 'BankAccount\BankAccountController',  ['only' => ['index', 'show']]);